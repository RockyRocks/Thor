#Comments
* Every feature must be heavily documented and relevant sources must be annotated.

#Reviews
* Each commit must be reviewed by one of the contributors.

#Features
* Each feature should be implemented keeping in mind that it should be extensible and relevant.