﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Sharpmake;

[module: Sharpmake.Include("Scripts\\common.sharpmake.cs")]
[module: Sharpmake.Include("Scripts\\kCommon.sharpmake.cs")]
[module: Sharpmake.Include("Scripts\\kCore.sharpmake.cs")]
[module: Sharpmake.Include("Scripts\\kEngine.sharpmake.cs")]
[module: Sharpmake.Include("Scripts\\kScriptEngine.sharpmake.cs")]

namespace sharpmake_debugSolution
{
    [Sharpmake.Generate]
    public class kSolution : Solution
    {
        public kSolution()
        {
            Name = "sbSolution";
            AddTargets(Common.CommonTarget);
        }

        [Configure()]
        public void ConfigureAll(Configuration conf, Target target)
        {
            if (target.OutputType == OutputType.Lib)
                conf.SolutionFileName = "sbSolution.[target.Framework].[target.Platform].[target.DevEnv]";
            else
                conf.SolutionFileName = "sbSolution.[target.Framework].[target.Platform].[target.OutputType].[target.DevEnv]";

            conf.SolutionPath = @"..\GeneratedProjects\";

            conf.AddProject<kCommon>(target);
            conf.AddProject<kCore>(target);
            conf.AddProject<kEngine>(target);
            conf.AddProject<kScriptEngine>(target);
        }
    }
    internal class kMain
    {
        [Main]
        public static void SharpmakeMain(Sharpmake.Arguments args)
        {
            args.Generate<kSolution>();
        }
    }
}
