﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO; // for Path.Combine
using Sharpmake; // contains the entire Sharpmake object library.
namespace sharpmake_debugSolution.Scripts
{
    [Generate]
    class kSystem : CommonProject
    {
        public kSystem() => Name = "kSystem";
        public override void ConfigureLib(Configuration conf, Target target)
        {
            base.ConfigureLib(conf, target);
            conf.Options.Add(Options.Vc.Compiler.Exceptions.EnableWithSEH);
        }
        public override void ConfigureDLL(Configuration conf, Target target)
        {
            base.ConfigureDLL(conf, target);
            conf.Defines.Add("K_SYSTEM_DLL");
            conf.Options.Add(Options.Vc.Compiler.Exceptions.EnableWithSEH);
        }
    }
}
