using System.IO; // for Path.Combine
using Sharpmake; // contains the entire Sharpmake object library.

namespace sharpmake_debugSolution
{
    [Generate]
    class kExtern : Project
    {
        public kExtern()
        {
            Name = "extern";
        }
    }
}