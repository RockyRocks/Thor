using System.IO; // for Path.Combine
using Sharpmake; // contains the entire Sharpmake object library.

namespace sharpmake_debugSolution
{
    [Generate]
    class kEngine : CommonProject
    {
        public kEngine()
        {
            Name = "kEngine";
        }

        public override void ConfigureLib(Configuration conf, Target target)
        {
            base.ConfigureLib(conf, target);
            conf.Options.Add(Options.Vc.Compiler.Exceptions.EnableWithSEH);
        }
        public override void ConfigureDLL(Configuration conf, Target target)
        {
            base.ConfigureDLL(conf, target);
            conf.Defines.Add("K_ENGINE_DLL");
            conf.Options.Add(Options.Vc.Compiler.Exceptions.EnableWithSEH);
        }
    }
}