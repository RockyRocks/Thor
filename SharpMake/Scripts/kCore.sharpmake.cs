using System.IO; // for Path.Combine
using Sharpmake; // contains the entire Sharpmake object library.

namespace sharpmake_debugSolution
{
    [Generate]
    public class kCore : CommonProject
    {
        public kCore()
        {
            Name = "kCore";
        }

        public override void ConfigureLib(Configuration conf, Target target)
        {
            base.ConfigureLib(conf, target);
            conf.Options.Add(Options.Vc.Compiler.Exceptions.EnableWithSEH);
        }
        public override void ConfigureDLL(Configuration conf, Target target)
        {
            base.ConfigureDLL(conf, target);
            conf.Defines.Add("K_CORE_DLL");
            conf.Options.Add(Options.Vc.Compiler.Exceptions.EnableWithSEH);
        }
    }
}
