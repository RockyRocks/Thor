﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Sharpmake;

namespace sharpmake_debugSolution
{
    public static class Common
    {
        // Adding vs2015 support only for now.
        public static Target[] CommonTarget = {
            new Target(
            Platform.win32 | Platform.win64,
            DevEnv.vs2017,
            Optimization.Debug | Optimization.Release,
            OutputType.Lib,
            Blob.Blob,
            BuildSystem.MSBuild,
            DotNetFramework.v4_6) ,

            new Target(
            Platform.win32 | Platform.win64,
            DevEnv.vs2017,
            Optimization.Debug | Optimization.Release,
            OutputType.Dll,
            Blob.Blob,
            BuildSystem.MSBuild,
            DotNetFramework.v4_6)
        };
        public static readonly string RootPath = @"..\..\Source";
        public static readonly string ExternRootPath = @"..\..\Extern";
        public static readonly string ToolsRootPath = @"..\..\Tools";
        public static readonly string BinRootPath = @"..\..\bin";
        public static readonly string ProjectsRootPath = @"..\..\GeneratedProjects";
    }
    public class CommonProject : Project
    {
        public CommonProject()
        {
            RootPath = Common.RootPath;
            SourceRootPath = @"[project.RootPath]\[project.Name]";
            AddTargets(Common.CommonTarget);
        }

        [Configure(OutputType.Lib)]
        public virtual void ConfigureLib(Configuration conf, Target target)
        {
            conf.Output = Configuration.OutputType.Lib;

            if (target.Optimization == Optimization.Debug)
                conf.Options.Add(Options.Vc.Compiler.RuntimeLibrary.MultiThreadedDebug);
            else
                conf.Options.Add(Options.Vc.Compiler.RuntimeLibrary.MultiThreaded);

            Configure(conf, target);
        }

        [Configure(OutputType.Dll)]
        public virtual void ConfigureDLL(Configuration conf, Target target)
        {
            conf.Output = Configuration.OutputType.Dll;

            if (target.Optimization == Optimization.Debug)
                conf.Options.Add(Options.Vc.Compiler.RuntimeLibrary.MultiThreadedDebugDLL);
            else
                conf.Options.Add(Options.Vc.Compiler.RuntimeLibrary.MultiThreadedDLL);

            Configure(conf, target);

        }
        private void Configure(Configuration conf, Target target)
        {
            conf.ProjectPath = Common.ProjectsRootPath + @"\[project.Name]\";
            conf.ProjectFileName = @"[project.Name][target.Framework].[target.DevEnv].[target.Platform]";
            conf.IntermediatePath = @"[conf.ProjectPath]\temp\[target.DevEnv]\[target.Platform]\[target]";
        }
    }
}
