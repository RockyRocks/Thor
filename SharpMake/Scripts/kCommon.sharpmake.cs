﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Sharpmake;

namespace sharpmake_debugSolution
{
    [Generate]
    class kCommon : CommonProject
    {
        public kCommon()
        {
            Name = "kCommon";
        }

        public override void ConfigureLib(Configuration conf, Target target)
        {
            base.ConfigureLib(conf, target);            
            conf.Options.Add(Options.Vc.Compiler.Exceptions.EnableWithSEH);
        }
        public override void ConfigureDLL(Configuration conf, Target target)
        {
            base.ConfigureDLL(conf, target);
            conf.Defines.Add("K_COMMON_DLL");            
            conf.Options.Add(Options.Vc.Compiler.Exceptions.EnableWithSEH);
        }
    }
}
