#pragma once

#ifndef K_SCRIPTENGINECONFIG_INCLUDED
#define K_SCRIPTENGINECONFIG_INCLUDED

#ifdef K_SCRIPTENGINECONFIG_DLL
    #if (K_PLATFORM != K_PLATFORM_WIN32) && (K_PLATFORM != K_PLATFORM_WIN64)
        #error K_SCRIPTENGINECONFIG_DLL is only configured for Win32 & Win64 platforms.
    #endif

    #define K_SCRIPTENGINECONFIG_DLL_IMPORT                 __declspec(dllimport)
    #define K_SCRIPTENGINECONFIG_DLL_EXPORT                 __declspec(dllexport)
#else
    #define K_SCRIPTENGINECONFIG_DLL_IMPORT
    #define K_SCRIPTENGINECONFIG_DLL_EXPORT
#endif
#endif //K_SCRIPTENGINECONFIG_INCLUDED