#pragma once

#ifndef K_CORECONFIG_INCLUDED
#define K_CORECONFIG_INCLUDED

#ifdef K_CORECONFIG_DLL
    #if (K_PLATFORM != K_PLATFORM_WIN32) && (K_PLATFORM != K_PLATFORM_WIN64)
        #error K_CORECONFIG_DLL is only configured for Win32 & Win64 platforms.
    #endif

    #define K_CORECONFIG_DLL_IMPORT                 __declspec(dllimport)
    #define K_CORECONFIG_DLL_EXPORT                 __declspec(dllexport)
#else
    #define K_CORECONFIG_DLL_IMPORT
    #define K_CORECONFIG_DLL_EXPORT
#endif
#endif //K_CORECONFIG_INCLUDED