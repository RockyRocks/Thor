/********************* All the generic defines should be here **************************/

// Unsigned base types

using U8                    = unsigned char;        // 8-bit  unsigned.
using U16                   = unsigned short int;   // 16-bit unsigned.
using U32                   = unsigned int;         // 32-bit unsigned.
using U64                   = unsigned long long;   // 64-bit unsigned.

// Signed base types.

using S8                    = signed char;          // 8-bit  signed.
using S16                   = signed short int;     // 16-bit signed.
using S32                   = signed int;           // 32-bit signed.
using S64                   = signed long long;     // 64-bit signed.

using Bool                  = U8;

constexpr auto TRUE         = 1;
constexpr auto FALSE        = 0;