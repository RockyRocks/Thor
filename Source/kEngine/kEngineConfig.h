#pragma once

#ifndef K_ENGINECONFIG_INCLUDED
#define K_ENGINECONFIG_INCLUDED

#ifdef K_ENGINECONFIG_DLL
    #if (K_PLATFORM != K_PLATFORM_WIN32) && (K_PLATFORM != K_PLATFORM_WIN64)
        #error K_ENGINECONFIG_DLL is only configured for Win32 & Win64 platforms.
    #endif

    #define K_ENGINECONFIG_DLL_IMPORT                 __declspec(dllimport)
    #define K_ENGINECONFIG_DLL_EXPORT                 __declspec(dllexport)
#else
    #define K_ENGINECONFIG_DLL_IMPORT
    #define K_ENGINECONFIG_DLL_EXPORT
#endif
#endif //K_ENGINECONFIG_INCLUDED