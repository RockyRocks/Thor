#pragma once

#ifndef K_COMMONCONFIG_INCLUDED
#define K_COMMONCONFIG_INCLUDED


#ifdef K_COMMON_DLL
    #if (K_PLATFORM != K_PLATFORM_WIN32) && (K_PLATFORM != K_PLATFORM_WIN64)
        #error K_COMMON_DLL is only configured for Win32 & Win64 platforms.
    #endif

    #define K_COMMON_DLL_IMPORT                 __declspec(dllimport)
    #define K_COMMON_DLL_EXPORT                 __declspec(dllexport)
#else
    #define K_COMMON_DLL_IMPORT
    #define K_COMMON_DLL_EXPORT
#endif

#endif //K_COMMONCONFIG_INCLUDED