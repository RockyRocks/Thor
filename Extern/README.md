# Extern

* Extern modules should be moved into this folder.

* All external libraries must be open source projects which would improve CI/CD in the long run. 

* Current proposed external libraries:

- bgfx/vulkan
- physx
- strip lumix engine?